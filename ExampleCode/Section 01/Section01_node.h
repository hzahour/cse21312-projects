/********************
 * Filename: Section01_node.h 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the node class
 * used for Linked Lists
 * ************************/

#ifndef SECTION01_NODE_H
#define SECTION01_NODE_H

template<class T>
class Node{
    
    public:
        /**************************
         * Function Name: Node<T>()
         * Preconditions: none 
         * Postconditions: none 
         * Generic constructor for node 
         * ************************/
        Node<T>();

        /**************************
         * Function Name: Node<T>()
         * Preconditions: T data 
         * Postconditions: none 
         * Constructor for node with data 
         * ************************/       
        Node<T>(const T data);

        /**************************
         * Function Name: Node<T>()
         * Preconditions: T data, Node<T>* next 
         * Postconditions: none 
         * Generic constructor for node and link  
         * ************************/      
        Node<T>(const T data, Node<T>* next);

        /**************************
         * Function Name: getNodeData
         * Preconditions: none 
         * Postconditions: T 
         * Returns the value of the private data 
         * ************************/        
        T getNodeData();

        /**************************
         * Function Name: getNodeLink 
         * Preconditions: none 
         * Postconditions: Node<T>*
         * Returns the pointer to the private link 
         * ************************/        
        Node<T>* getNodeLink();

        /**************************
         * Function Name: setNodeData
         * Preconditions: T 
         * Postconditions: none 
         * Sets the value of the node 
         * ************************/        
        void setNodeData(T newData);

        /**************************
         * Function Name: setLinkData
         * Preconditions: Node<T>* 
         * Postconditions: none 
         * Sets the value of the link 
         * ************************/          
        void setNodeLink(Node<T>* newLink);
    
    private:
        // The actual data element of the node
        T data;
        // The link (pointer) between each node 
        Node<T>* link;
    
};

// Class Implementations

template<class T>
Node<T>::Node() : link(nullptr) {
    // We are not explicitly defining the node data 
    // because we do not know the type until compile time.
}

template<class T>
Node<T>::Node(const T data) : data(data), link(nullptr) { }

template<class T>
Node<T>::Node(const T data, Node<T>* next) : data(data), link(next) { }

template<class T>
T Node<T>::getNodeData(){
    return data;
}

template<class T>
Node<T>* Node<T>::getNodeLink(){
    return link;
}

template<class T>
void Node<T>::setNodeData(T newData){
    data = newData;
}

template<class T>
void Node<T>::setNodeLink(Node<T>* newLink){
    link = newLink;
}

#endif